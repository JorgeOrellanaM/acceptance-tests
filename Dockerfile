FROM docker.jala.pro/devops/openjdk:8-jdk-alpine AS build
WORKDIR /app

# Copy Gradle project
COPY build.gradle .
COPY src src/
COPY gradle gradle/
COPY gradlew .

# Build Gradlew Java test project.
RUN ./gradlew test --rerun-tasks --info
